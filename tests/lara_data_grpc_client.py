import logging
logging.basicConfig()

import grpc

import lara_django_data.lara_data_pb2 as lara_data_pb2
import lara_django_data.lara_data_pb2_grpc as lara_data_pb2_grpc

def run():
    # NOTE(gRPC Python Team): .close() is possible on a channel and should be
    # used in circumstances in which the with statement does not fit the needs
    # of the code.
    with grpc.insecure_channel('localhost:50051') as channel:
        stub = lara_data_pb2_grpc.LaraDataStub(channel)
        response = stub.RetrieveData(lara_data_pb2.RetrieveDataRequest(name_full=""))
    print("Server data received: " + response.data_JSON )

run()
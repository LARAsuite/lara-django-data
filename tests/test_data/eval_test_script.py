print("External: reverse order dyn_test_progr")

cl_obj1 = Evaluate(None, parameters="ext p1")

cl_obj2 = Evaluate(cl_obj1, parameters="ext p2")

cl_obj3 = Evaluate(cl_obj2, parameters="ext p3")

logging.debug("\n\n------ printing {} --------\n".format("Parameters") )

cl_obj3.recPrintParameters()

logging.debug("\n\n------ evaluating {} --------\n".format("Bottom - > Top ") )
cl_obj3.evalBottomTop()
logging.debug("\n\n------ evaluating {} --------\n".format("Top -> Bottom") )
cl_obj3.evalTopBottom()

logging.debug("ext cl_ob 1 para is: {}".format(cl_obj1.parameters) )

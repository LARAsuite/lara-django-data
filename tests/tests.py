"""_____________________________________________________________________

:PROJECT: LARA

*lara_people tests *

:details: lara_people application tests.
         - 

:file:    tests.py
:authors: 

:date: (creation)          
:date: (last modification) 

.. note:: -
.. todo:: - 
________________________________________________________________________
"""
__version__ = "0.0.1"

import logging

logging.basicConfig(format='%(levelname)s| %(module)s.%(funcName)s:%(message)s', level=logging.DEBUG)
#~ logging.basicConfig(format='%(levelname)s| %(module)s.%(funcName)s:%(message)s', level=logging.ERROR)

# Create your tests here. s. https://docs.djangoproject.com/en/1.9/intro/tutorial05/

import datetime

from django.utils import timezone
from django.test import TestCase
from django.urls import reverse

from ..models import Data

def createData():
    """
    Creates an Data
    """
    pass
    #return Data.objects.create(

    # data_type = models.ForeignKey( DataType, 
    #                                 related_name='data_class_related', 
    #                                 on_delete=models.CASCADE, blank=True, null=True, 
    #                                 help_text="data type of this data item, e.g.  FASTA, AB1, AnIML, ...")
    # namespace = models.ForeignKey(Namespace, related_name="%(app_label)s_%(class)s_namespace_related", 
    #                   related_query_name="%(app_label)s_%(class)s_namespace",
    #                   on_delete=models.CASCADE, null=True, blank=True, 
    #                   help_text="namespace of data")
    # name = models.TextField(unique=True, blank=True, 
    #                         help_text="name of the data")
    # version = models.TextField(blank=True, default="0.1.0", help_text="maj.min version of this visualisation, e.g. 0.1.0")
    # name_full = models.TextField(unique=True, blank=True, 
    #                              help_text="fully qualified name of the data, including namespace and version; the name_full should only contain ASCII-alpha-numeric characters, no white-spaces, '-', '_' and '.' .")
    # UUID = models.UUIDField(default=uuid.uuid4, help_text="data UUID")
    # # it might be more memory efficient to store the hash in binary format
    # hash_SHA256 = models.CharField(max_length=256, unique=True, blank=True, null=True, default=None, 
    #                                   help_text="SHA256 hash of all data (JSON and XML)")
    # datetime_created = models.DateTimeField(default=timezone.now, help_text="date and time when data was created")
    # method = models.ForeignKey(MethodInstance, related_name='method_data', 
    #                            on_delete=models.CASCADE, blank=True, null=True, help_text="measurement or evaluation method, like SeqDNA, SeqProtein, SPabs, ...")
    # process = models.ForeignKey(ProcessInstance, related_name='process_data', on_delete=models.CASCADE, blank=True, null=True, 
    #                             help_text="process that generated the data, the process contains the procedures.")
    # data_JSON = models.JSONField(blank=True, null=True, help_text="JSON representation of the data")
    # data_XML = models.TextField(blank=True,  null=True, help_text="XML representation of the data")
    # data_BLOB = models.BinaryField(blank=True,  null=True, help_text="binary representation of the data - for caching of data, handle with care !")
    # visualisations = models.ManyToManyField(Visualisation, related_name="%(app_label)s_%(class)s_visualisation_related", 
    #                                         related_query_name="%(app_label)s_%(class)s_visualisations",  blank=True, 
    #                                         help_text="visualisations of the data")
    # URI = models.TextField(blank=True,  null=True, help_text="gneneric URI to the data")
    # handle = models.URLField(blank=True,  null=True, help_text="handle system URI to the data")
    # DOI = models.URLField(blank=True,  null=True, help_text="Digital Object Identifier to the data")
    # filename = models.FileField(upload_to='lara_data/', blank=True, null=True, help_text="rel. path/filename")
    # literature = models.ManyToManyField(LibItem, related_name="%(app_label)s_%(class)s_literature_related", 
    #                                     related_query_name="%(app_label)s_%(class)s_literature",  blank=True, 
    #                                     help_text="literature regarding this instance of a substance")
    # icon = models.TextField(blank=True, null=True, help_text="XML/SVG icon / symbolising the data")
    # tags = models.ManyToManyField(Tag, related_name="%(app_label)s_%(class)s_tag_related", 
    #                             related_query_name="%(app_label)s_%(class)s_tag", blank=True, help_text="tags")
    # description = models.TextField(blank=True, null=True, help_text="description of the data")
    # )

class DataMethodsTests(TestCase):
    def testData(self):
        """
        Testing Data 
        """
        pass
       # createData()
        
        #~ print Data.objects.get(Data_id=1)
        
        #datum = Data.objects.get(Data_id=1).street
        
        #self.assertEqual(data,"astreet" )


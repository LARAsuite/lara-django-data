"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_data tests *

:details: lara_django_data application urls tests.
         - 
:authors: mark doerr  <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - 
________________________________________________________________________
"""

from django.test import TestCase
from django.urls import resolve, reverse

# from lara_django_data.models import

# Create your lara_django_data tests here.

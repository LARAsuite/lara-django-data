import grpc
import lara_django_data.lara_data_pb2
import lara_django_data.lara_data_pb2_grpc


with grpc.insecure_channel('localhost:50051') as channel:
    stub = lara_django_data.lara_data_pb2_grpc.DataControllerStub(channel)
    for datum in stub.List(lara_django_data.lara_data_pb2.DataRequest()):
        print("datum:",datum.name, end='')
    
    #resp = lara_data.lara_data_pb2.DataRequest(1)
    #print(resp)

    #for datum in stub.List(lara_data.lara_data_pb2.DataRequest()):
    #    print("datum:",datum.name, datum.data_XML, end='')
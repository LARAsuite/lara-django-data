# gRPC setup


LARA gRPC support is based on (a maintained fork of grpc-framework):
https://github.com/socotecio/django-socio-grpc


python manage.py generateproto --model lara_django_data.Data --fields data_JSON,data_XML description --file data.proto

python -m grpc_tools.protoc --proto_path=./ --python_out=./ --grpc_python_out=./ ./account.proto


python -m grpc_tools.protoc --proto_path=./protos --python_out=./grpc_if --grpc_python_out=./grpc_if lara_data.proto




lara-django-dev grpcrunserver --dev
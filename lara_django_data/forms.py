"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_data admin *

:details: lara_django_data admin module admin backend configuration.
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - run "lara-django-dev forms_generator -c lara_django_data > forms.py" to update this file
________________________________________________________________________
"""
# django crispy forms s. https://github.com/django-crispy-forms/django-crispy-forms for []
# generated with django-extensions forms_generator -c  [] > forms.py (LARA-version)

from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, Row, Column

from .models import ExtraData, Visualisation, Data, Evaluation


class ExtraDataCreateForm(forms.ModelForm):
    class Meta:
        model = ExtraData
        fields = (
            'data_type',
            'namespace',
            'URI',
            'text',
            'XML',
            'JSON',
            'media_type',
            'IRI',
            'URL',
            'description',
            'file',
            'image')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'data_type',
            'namespace',
            'URI',
            'text',
            'XML',
            'JSON',
            'media_type',
            'IRI',
            'URL',
            'description',
            'file',
            'image',
            Submit('submit', 'Create')
        )


class ExtraDataUpdateForm(forms.ModelForm):
    class Meta:
        model = ExtraData
        fields = (
            'data_type',
            'namespace',
            'URI',
            'text',
            'XML',
            'JSON',
            'media_type',
            'IRI',
            'URL',
            'description',
            'file',
            'image')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'data_type',
            'namespace',
            'URI',
            'text',
            'XML',
            'JSON',
            'media_type',
            'IRI',
            'URL',
            'description',
            'file',
            'image',
            Submit('submit', 'Update')
        )


class VisualisationCreateForm(forms.ModelForm):
    class Meta:
        model = Visualisation
        fields = (
            'namespace',
            'name',
            'version',
            'name_full',
            'data_type',
            'plotting_method',
            'plotting_procedure',
            'rendering_html_template',
            'rendering_css_template',
            'rendering_component',
            'IRI',
            'caption',
            'thumbnail',
            'description')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'namespace',
            'name',
            'version',
            'name_full',
            'data_type',
            'plotting_method',
            'plotting_procedure',
            'rendering_html_template',
            'rendering_css_template',
            'rendering_component',
            'IRI',
            'caption',
            'thumbnail',
            'description',
            Submit('submit', 'Create')
        )


class VisualisationUpdateForm(forms.ModelForm):
    class Meta:
        model = Visualisation
        fields = (
            'namespace',
            'name',
            'version',
            'name_full',
            'data_type',
            'plotting_method',
            'plotting_procedure',
            'rendering_html_template',
            'rendering_css_template',
            'rendering_component',
            'IRI',
            'caption',
            'thumbnail',
            'description')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'namespace',
            'name',
            'version',
            'name_full',
            'data_type',
            'plotting_method',
            'plotting_procedure',
            'rendering_html_template',
            'rendering_css_template',
            'rendering_component',
            'IRI',
            'caption',
            'thumbnail',
            'description',
            Submit('submit', 'Update')
        )


class DataCreateForm(forms.ModelForm):
    class Meta:
        model = Data
        fields = (
            'data_type',
            'media_type',
            'namespace',
            'name',
            'version',
            'name_full',
            'UUID',
            'hash_SHA256',
            'datetime_created',
            'method',
            'process',
            'process_calibration',
            'data_JSON',
            'data_XML',
            'metadata',
            'URI',
            'IRI',
            'handle',
            'DOI',
            'filename',
            'literature',
            'icon',
            'description')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'data_type',
            'media_type',
            'namespace',
            'name',
            'version',
            'name_full',
            'UUID',
            'hash_SHA256',
            'datetime_created',
            'method',
            'process',
            'process_calibration',
            'data_JSON',
            'data_XML',
            'metadata',
            'URI',
            'IRI',
            'handle',
            'DOI',
            'filename',
            'literature',
            'icon',
            'description',
            Submit('submit', 'Create')
        )


class DataUpdateForm(forms.ModelForm):
    class Meta:
        model = Data
        fields = (
            'data_type',
            'media_type',
            'namespace',
            'name',
            'version',
            'name_full',
            'UUID',
            'hash_SHA256',
            'datetime_created',
            'method',
            'process',
            'process_calibration',
            'data_JSON',
            'data_XML',
            'metadata',
            'URI',
            'IRI',
            'handle',
            'DOI',
            'filename',
            'literature',
            'icon',
            'description')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'data_type',
            'media_type',
            'namespace',
            'name',
            'version',
            'name_full',
            'UUID',
            'hash_SHA256',
            'datetime_created',
            'method',
            'process',
            'process_calibration',
            'data_JSON',
            'data_XML',
            'metadata',
            'URI',
            'IRI',
            'handle',
            'DOI',
            'filename',
            'literature',
            'icon',
            'description',
            Submit('submit', 'Update')
        )


class EvaluationCreateForm(forms.ModelForm):
    class Meta:
        model = Evaluation
        fields = (
            'namespace',
            'name',
            'name_full',
            'version',
            'UUID',
            'hash_SHA256',
            'parameters',
            'eval_method',
            'eval_procedure',
            'URI',
            'IRI',
            'handle',
            'DOI',
            'literature',
            'caption',
            'description')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'namespace',
            'name',
            'name_full',
            'version',
            'UUID',
            'hash_SHA256',
            'parameters',
            'eval_method',
            'eval_procedure',
            'URI',
            'IRI',
            'handle',
            'DOI',
            'literature',
            'caption',
            'description',
            Submit('submit', 'Create')
        )


class EvaluationUpdateForm(forms.ModelForm):
    class Meta:
        model = Evaluation
        fields = (
            'namespace',
            'name',
            'name_full',
            'version',
            'UUID',
            'hash_SHA256',
            'parameters',
            'eval_method',
            'eval_procedure',
            'URI',
            'IRI',
            'handle',
            'DOI',
            'literature',
            'caption',
            'description')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'namespace',
            'name',
            'name_full',
            'version',
            'UUID',
            'hash_SHA256',
            'parameters',
            'eval_method',
            'eval_procedure',
            'URI',
            'IRI',
            'handle',
            'DOI',
            'literature',
            'caption',
            'description',
            Submit('submit', 'Update')
        )

# from .forms import ExtraDataCreateForm, VisualisationCreateForm, DataCreateForm, EvaluationCreateFormExtraDataUpdateForm, VisualisationUpdateForm, DataUpdateForm, EvaluationUpdateForm

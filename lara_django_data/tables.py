"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_data admin *

:details: lara_django_data admin module admin backend configuration.
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - run "lara-django-dev tables_generator lara_django_data >> tables.py" to update this file
________________________________________________________________________
"""
# django Tests s. https://docs.djangoproject.com/en/4.1/topics/testing/overview/ for lara_django_data
# generated with django-extensions tests_generator  lara_django_data > tests.py (LARA-version)

import logging
import django_tables2 as tables

from .models import ExtraData, Visualisation, Data, Evaluation


class ExtraDataTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_data:extradata-detail', [tables.A('pk')]))

    class Meta:
        model = ExtraData

        fields = (
            'data_type',
            'namespace',
            'URI',
            'text',
            'XML',
            'JSON',
            'bin',
            'media_type',
            'IRI',
            'URL',
            'description',
            'file',
            'image')


class VisualisationTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_data:visualisation-detail', [tables.A('pk')]))

    class Meta:
        model = Visualisation

        fields = (
            'namespace',
            'name',
            'version',
            'name_full',
            'data_type',
            'plotting_method',
            'plotting_procedure',
            'rendering_html_template',
            'rendering_css_template',
            'rendering_component',
            'IRI',
            'caption',
            'thumbnail',
            'description')


class DataTable(tables.Table):
    # adding link to column <column-to-be-linked>
    name = tables.Column(linkify=('lara_django_data:data-detail', [tables.A('pk')]))

    class Meta:
        model = Data

        fields = (
            
            'namespace',
            'name',
            'version',
            'data_type',
            'media_type',
           
            'datetime_created',
            'method',
            'process',
            'process_calibration',
         
            'metadata',
          
            'filename',
            'literature',
            'icon',
            'description')


class EvaluationTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_data:evaluation-detail', [tables.A('pk')]))

    class Meta:
        model = Evaluation

        fields = (
            'namespace',
            'name',
            'name_full',
            'version',
            'UUID',
            'hash_SHA256',
            'parameters',
            'eval_method',
            'eval_procedure',
            'URI',
            'IRI',
            'handle',
            'DOI',
            'literature',
            'caption',
            'description')

"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_data models *

:details: lara_django_data database models.
          The data table is generically used to store many kinds of data.
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - remove unwanted models/fields
           - add gRPC interface
          - in long the term
           a Blockchain kind of data storage might be implemented
           for better reliability.
________________________________________________________________________
"""

import logging
import hashlib
import uuid
import json

from django.db import models
from django.conf import settings
from django.utils import timezone

from lara_django_base.models import DataType, ExtraDataAbstr, MediaType, Namespace, Tag
from lara_django_library.models import LibItem

from lara_django_processes.models import Method, Process, Procedure, \
    MethodInstance, ProcessInstance, ProcedureInstance


settings.FIXTURES += []


class ExtraData(ExtraDataAbstr):
    """This class can be used to extend data, by extra information,
       e.g. more telephone numbers, customer numbers, ... """

    extra_data_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)  # models.AutoField(primary_key=True)
    file = models.FileField(
        upload_to='data', blank=True, null=True, help_text="rel. path/filename")
    image = models.ImageField(upload_to='data/images/', blank=True, default="image.svg",
                              help_text="location room map rel. path/filename to image")


class Visualisation(models.Model):
    """Information and MetaInformation about visualisation of Data shall be stored here, like the method/procedure of plotting.
       This can be used to specify default visualisation routines for certain data types, 
       e.g. bacterial growth data can be plotted with a 2D growth plot or heatmap.
       The name_full should only contain ASCII-alpha-numeric characters, no white-spaces, '-', '_' and '.' .
       For versioning semantic versioning (https://semver.org/lang/en/) shall be used. 
    """
    #id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    visualisation_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    namespace = models.ForeignKey(Namespace, related_name="%(app_label)s_%(class)s_namespaces_related",
                                  related_query_name="%(app_label)s_%(class)s_namespaces_related_query",
                                  on_delete=models.CASCADE, null=True, blank=True,
                                  help_text="namespace of visualisation")
    name = models.TextField(unique=True, blank=True,
                            help_text="name of the visualisation")
    version = models.TextField(blank=True, default="0.1.0",
                               help_text="maj.min.patch version of this visualisation, e.g. 0.1.0")
    name_full = models.TextField(unique=True, blank=True,
                                 help_text="fully qualified name of the visualisation, including namespace and version; the name_full should only contain ASCII-alpha-numeric characters, no white-spaces, '-', '_' and '.' .")
    data_type = models.ForeignKey(DataType, related_name="%(app_label)s_%(class)s_data_type_related",
                                  on_delete=models.CASCADE, null=True, blank=True,
                                  help_text="associate a data type to this method")
    plotting_method = models.ForeignKey(MethodInstance, related_name="%(app_label)s_%(class)s_plotting_methods_related",
                                        on_delete=models.CASCADE, null=True, blank=True,
                                        help_text="plotting method to generate a visualisation")
    plotting_procedure = models.ForeignKey(ProcedureInstance, related_name="%(app_label)s_%(class)s_plotting_procedures_related",
                                           on_delete=models.CASCADE, null=True, blank=True,
                                           help_text="plotting procedure to generate a visualisation")
    rendering_html_template = models.TextField(
        blank=True, help_text="custom html template e.g. a DJANGO template for rendering")
    rendering_css_template = models.TextField(
        blank=True, help_text="custom css of custom html template e.g. a DJANGO template")
    rendering_component = models.TextField(
        blank=True, help_text="custom javascript framework components, like Vue.js components for rendering")
    IRI = models.URLField(
        blank=True,  null=True, unique=True, max_length=512, help_text="International Resource Identifier - IRI: is used for semantic representation ")
    tags = models.ManyToManyField(Tag, related_name="%(app_label)s_%(class)s_tags_related",
                                  related_query_name="%(app_label)s_%(class)s_tags_related_query", blank=True, help_text="tags to classify the visualisation")
    caption = models.TextField(
        blank=True, null=True, help_text="figure or table caption")
    thumbnail = models.TextField(
        blank=True, null=True, help_text="XML/SVG icon / symbolising the data")
    description = models.TextField(
        blank=True, null=True, help_text="description of the visualisation")

    def __str__(self):
        return self.data_type or ""

    def __repr__(self):
        return self.data_type or ""

    # def save(self, force_insert=None, using=None): #force_insert=force_insert, using=using
    def save(self, *args, **kwargs):
        """
        Here we generate some default values for name_full
        """
        if not self.name_full:
            self.name_full = '/'.join(
                (self.namespace.name, self.name, self.version))

        super().save(*args, **kwargs)


class Data(models.Model):
    """Generic data class. All data, like data from experiments, measurements or evaluations will be stored here.
       There are also references to the way, how the data is produced, evaluated and visualised.
       The data contains a reference to the process which generated the data.
       For versioning semantic versioning (https://semver.org/lang/en/) shall be used. 
       .. todo:: - check, whether UUID is required, if hash is there...
                 - Blockchain data
    """
    #id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    data_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    data_type = models.ForeignKey(DataType,
                                  related_name='%(app_label)s_%(class)s_data_types_related',
                                  related_query_name="%(app_label)s_%(class)s_data_types_related_query",
                                  on_delete=models.CASCADE, blank=True, null=True,
                                  help_text="data type of this data item, e.g.  FASTA, AB1, AnIML, ...")
    media_type = models.ForeignKey(MediaType,
                                   related_name='%(app_label)s_%(class)s_data_types_related',
                                   related_query_name="%(app_label)s_%(class)s_data_types_related_query",
                                   on_delete=models.CASCADE, blank=True, null=True,
                                   help_text="IANA data media type of this data item, e.g. txt/xml, txt/csv, ......")
    namespace = models.ForeignKey(Namespace, related_name="%(app_label)s_%(class)s_namespaces_related",
                                  related_query_name="%(app_label)s_%(class)s_namespaces_related_query",
                                  on_delete=models.CASCADE, null=True, blank=True,
                                  help_text="namespace of data")
    name = models.TextField(unique=True, blank=True,
                            help_text="name of the data")
    version = models.TextField(blank=True, default="0.1.0",
                               help_text="maj.min version of this visualisation, e.g. 0.1.0")
    name_full = models.TextField(unique=True, blank=True,
                                 help_text="fully qualified name of the data, including namespace and version; the name_full should only contain ASCII-alpha-numeric characters, no white-spaces, '-', '_' and '.' .")
    UUID = models.UUIDField(default=uuid.uuid4, help_text="data UUID")
    # it might be more memory efficient to store the hash in binary format
    hash_SHA256 = models.CharField(max_length=256, unique=True, blank=True, null=True, default=None,
                                   help_text="SHA256 hash of all data (JSON and XML)")
    datetime_created = models.DateTimeField(
        default=timezone.now, help_text="date and time when data was created")
    #datetime_last_modified = models.DateTimeField(default=timezone.now, help_text="date and time when data was lasst modified")
    method = models.ForeignKey(MethodInstance, related_name='%(app_label)s_%(class)s_methods_related',
                               related_query_name="%(app_label)s_%(class)s_methods_related_query",
                               on_delete=models.CASCADE, blank=True, null=True, help_text="measurement or evaluation method, like SeqDNA, SeqProtein, SPabs, ...")
    process = models.ForeignKey(ProcessInstance, related_name='%(app_label)s_%(class)s_processes_related',
                                related_query_name="%(app_label)s_%(class)s_processes_related_query", on_delete=models.CASCADE, blank=True, null=True,
                                help_text="process that generated the data, the process contains the procedures.")
    process_calibration = models.ForeignKey(ProcessInstance, related_name='%(app_label)s_%(class)s_processes_calibration_related',
                                            related_query_name="%(app_label)s_%(class)s_processes_calibration_related_query", on_delete=models.CASCADE, blank=True, null=True,
                                            help_text="calibration process for the measurement process that generated the data, the process contains the procedures.")
    data_JSON = models.JSONField(
        blank=True, null=True, help_text="JSON representation of the data")
    data_XML = models.TextField(
        blank=True,  null=True, help_text="XML representation of the data")
    data_BLOB = models.BinaryField(
        blank=True,  null=True, help_text="binary representation of the data - for caching of data, handle with care !")
    data_calibration_BLOB = models.BinaryField(
        blank=True,  null=True, help_text="binary representation of the calibration data/parameters")
    metadata = models.JSONField(
        blank=True, null=True, help_text="metadata, like units, visualisation factor/unit")
    visualisations = models.ManyToManyField(Visualisation, related_name="%(app_label)s_%(class)s_visualisation_related",
                                            related_query_name="%(app_label)s_%(class)s_visualisations_related_query",  blank=True,
                                            help_text="visualisations of the data")
    URI = models.TextField(blank=True,  null=True,
                           help_text="gneneric URI to the data")
    # semantics -> RDF database
    IRI = models.URLField(
        blank=True,  null=True, unique=True, max_length=512, help_text="International Resource Identifier to instance of OWL class: is used for semantic representation ")
    handle = models.URLField(
        blank=True, null=True,  unique=True, max_length=512, help_text=" [handle URI](https://www.handle.net/)")
    DOI = models.URLField(blank=True,  null=True,
                          help_text="Digital Object Identifier to the data")
    filename = models.FileField(
        upload_to='lara_data/', blank=True, null=True, help_text="rel. path/filename")
    # literature = models.ManyToManyField(LibItem, related_name="%(app_label)s_%(class)s_literature_related",
    #                                     related_query_name="%(app_label)s_%(class)s_literature_related_query",  blank=True,
    #                                     help_text="literature regarding this instance of a substance")
    # replace by a ManyToManyField to URLs
    literature = models.URLField(
        blank=True, null=True, help_text="URL to literature regarding this instance of an evaluation")
    icon = models.TextField(blank=True, null=True,
                            help_text="XML/SVG icon / symbolising the data")
    tags = models.ManyToManyField(Tag, related_name="%(app_label)s_%(class)s_tags_related",
                                  related_query_name="%(app_label)s_%(class)s_tags_related_query", blank=True, help_text="tags")
    description = models.TextField(
        blank=True, null=True, help_text="description of the data")

    class Meta:
        verbose_name_plural = "Data"

    def __str__(self):
        return self.name_full or ""

    def __repr__(self):
        return " : ".join((self.name_full, self.description)) or ""

    # def save(self, force_insert=None, using=None): #force_insert=force_insert, using=using
    def save(self, *args, **kwargs):
        """
        Here we generate some default values for name_full
        """
        if not self.hash_SHA256:
            to_hash = str(self.UUID) + json.dumps(self.data_JSON)+self.data_XML
            self.hash_SHA256 = hashlib.sha256(
                to_hash.encode('utf-8')).hexdigest()

        if not self.name:
            self.name = '_'.join(
                (timezone.now().strftime('%Y%m%d_%H%M%S'), self.hash_SHA256[:8]))
        if not self.name_full:
            self.name_full = '/'.join(
                (self.namespace.name, self.name, self.version))

        if not self.URI:
            self.URI = 'data://' + self.name_full

        super().save(*args, **kwargs)

    # def to_protobuf(self):
    #    return page_pb2.Data(name=self.name, data_JSON=self.data_JSON)


class Evaluation(models.Model):
    """This class can be used to describe evaluation information.
       The name_full should only contain ASCII-alpha-numeric characters, no white-spaces, '-', '_' and '.' .
       For versioning semantic versioning (https://semver.org/lang/en/) shall be used.
        .. todo:: 
            - auto hashing
            - ensure that data and results cannot be modified ? (by hash checking)
            - method versioning
            - due to an unknown reason it is impossible to feedback on experiments (generates circular dependancy error). 
              This should be fixed in a later version of the database
    """
    #id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    eval_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)

    namespace = models.ForeignKey(Namespace, related_name="%(app_label)s_%(class)s_namespaces_related",
                                  related_query_name="%(app_label)s_%(class)s_namespaces_related_query",
                                  on_delete=models.CASCADE, null=True, blank=True,
                                  help_text="namespace of evaluation")
    name = models.TextField(unique=True, blank=True,
                            help_text="name of the evaluation")
    name_full = models.TextField(unique=True, blank=True,
                                 help_text="fully qualified name of the evaluation, including namespace and version, separated by a period. The name_full should only contain ASCII-alpha-numeric characters, no white-spaces, '-', '_' and '.' .")
    version = models.TextField(blank=True, default="0.1.0",
                               help_text="maj.min.patch version of this visualisation, e.g. 0.1.0")
    UUID = models.UUIDField(default=uuid.uuid4, help_text="evaluation UUID")
    # it might be more memory efficient to store it in binary format
    hash_SHA256 = models.CharField(max_length=256, unique=True, blank=True, null=True, default=None,
                                   help_text="SHA256 hash of all hashes of eval methods and data")
    # parameters need to have a JSON scheme (!)
    parameters = models.JSONField(
        blank=True, null=True, help_text="parameters used for evaluation, e.g. left and right limits, etc.")
    #  check if linked list can be used here ?
    # -> ltree will be modelled with the PostgreSQL ltree https://pypi.org/project/django-ltree/
    parent_evals = models.ManyToManyField('self',
                                          related_query_name="%(app_label)s_%(class)s_parent_evals_related",  blank=True,
                                          help_text="to define a tree/network of evaluations and to go back in the evaluation process")
    # dependencies
    # eval tree/graph/network
    eval_method = models.ForeignKey(MethodInstance, related_name="%(app_label)s_%(class)s_eval_methods_related",
                                    on_delete=models.CASCADE, null=True, blank=True,
                                    help_text="evaluation method")
    eval_procedure = models.ForeignKey(ProcedureInstance, related_name="%(app_label)s_%(class)s_eval_procs_related",
                                       on_delete=models.CASCADE, null=True, blank=True,
                                       help_text="evaluation procedure")
    eval_data = models.ManyToManyField(Data, related_name="%(app_label)s_%(class)s_eval_data_related",
                                       related_query_name="%(app_label)s_%(class)s_eval_data_related_query", blank=True,
                                       help_text="all data that is used for this evaluation")
    results_data = models.ManyToManyField(Data, related_name="%(app_label)s_%(class)s_results_data_related",
                                          related_query_name="%(app_label)s_%(class)s_results_data_related_query", blank=True,
                                          help_text="all data that is generated by this evaluation")
    visualisations = models.ManyToManyField(Visualisation, related_name="%(app_label)s_%(class)s_visualisations_related",
                                            related_query_name="%(app_label)s_%(class)s_visualisations_related_query",  blank=True,
                                            help_text="visualisations of this evaluated data")
    URI = models.TextField(blank=True,  null=True,
                           help_text="gneneric URI to the data")
    IRI = models.URLField(
        blank=True,  null=True, unique=True, help_text="International Resource Identifier - IRI: is used for semantic representation ")
    handle = models.URLField(
        blank=True, null=True,  unique=True, help_text=" [handle URI](https://www.handle.net/)")
    DOI = models.URLField(blank=True,  null=True,
                          help_text="Digital Object Identifier to the data")
    # references
    # literature = models.ManyToManyField(LibItem, related_name="%(app_label)s_%(class)s_literature_related",
    #                                     related_query_name="%(app_label)s_%(class)s_literature",  blank=True,
    #                                     help_text="references to literature regarding this instance of an evaluation")
    # replace by a ManyToManyField to URLs
    literature = models.URLField(
        blank=True, null=True, help_text="URL to literature regarding this instance of an evaluation")
    tags = models.ManyToManyField(Tag, related_name="%(app_label)s_%(class)s_tags_related",
                                  related_query_name="%(app_label)s_%(class)s_tags_related_query", blank=True, help_text="tags, characterising the evaluation")
    caption = models.TextField(
        blank=True, null=True, help_text="can be used as table caption")
    description = models.TextField(
        blank=True, null=True, help_text="description")

    def __str__(self):
        return self.name_full or ""

    def __repr__(self):
        return ' '.join((self.name_full, self.description)) or ""

    # def save(self, force_insert=None, using=None): #force_insert=force_insert, using=using
    def save(self, *args, **kwargs):
        """
        Here we generate some default values for name_full
        """
        if not self.name_full:
            self.name_full = '.'.join(
                (self.namespace.name, self.name, self.version))

        if not self.hash_SHA256:
            self.hash_SHA256 = hashlib.sha256(
                self.eval_procedure.hash_SHA256).hexdigest()

        super().save(*args, **kwargs)

"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_data views *

:details: lara_django_data views module.
         - add app specific urls here
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: -
________________________________________________________________________
"""

from .models import Data, Evaluation
from django.views.generic import DetailView
from django.views.generic import ListView
from django.shortcuts import get_object_or_404, render
import sys
from dataclasses import dataclass, field
from typing import List

from django.shortcuts import get_object_or_404, render, redirect
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse

from django.views.generic import DetailView, ListView, CreateView, UpdateView, DeleteView

from django_tables2 import SingleTableView

from .models import Data
from .forms import DataCreateForm, DataUpdateForm
from .tables import DataTable

# Create your  lara_django_data views here.


@dataclass
class DataMenu:
    menu_items:  List[dict] = field(default_factory=lambda: [   
        {'name': 'Data',
         'path': 'lara_django_data:data-list'},
    ])




class DataSingleTableView(SingleTableView):
    model = Data
    table_class = DataTable

    #fields = ('name', 'name_full', 'URL', 'handle', 'IRI', 'manufacturer', 'model_no', 'product_type', 'type_barcode', 'product_no', 'weight', 'specifications', 'spec_JSON', 'spec_doc', 'brochure', 'quickstart', 'manual', 'service_manual', 'icon', 'image', 'description', 'data_id', 'data_class', 'shape')

    template_name = 'lara_django_data/list.html'
    success_url = '/data/data/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Data - List"
        context['create_link'] = 'lara_django_data:data-create'
        context['menu_items'] = DataMenu().menu_items
        return context


class DataDetailView(DetailView):
    model = Data

    template_name = 'lara_django_data/data_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Data - Details"
        context['update_link'] = 'lara_django_data:data-update'
        context['menu_items'] = DataMenu().menu_items
        return context


class DataCreateView(CreateView):
    model = Data

    template_name = 'lara_django_data/create_form.html'
    form_class = DataCreateForm
    success_url = '/data/data/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Data - Create"
        return context


class DataUpdateView(UpdateView):
    model = Data

    template_name = 'lara_django_data/update_form.html'
    form_class = DataUpdateForm
    success_url = '/data/data/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Data - Update"
        context['delete_link'] = 'lara_django_data:data-delete'
        return context


class DataDeleteView(DeleteView):
    model = Data

    template_name = 'lara_django_data/delete_form.html'
    success_url = '/data/data/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Data - Delete"
        context['delete_link'] = 'lara_django_data:data-delete'
        return context


## old

class IndexView(ListView):
    model = Data
    template_name = 'lara_data/index.html'

    def get_queryset(self):
        """Return the lastest 25 data sets."""
        return Data.objects.order_by('-start_datetime')[:25]


class DataListView(ListView):
    model = Data

    def get_context_data(self, **kwargs):
        context = super(DataListView, self).get_context_data(**kwargs)

        context['table_caption'] = "List of all data"
        return context


def searchForm(request):
    return render(request, 'lara_data/search.html', context={})


def search(request):
    search_string = request.POST['search']

    try:
        data_list = []
        curr_data = Data.objects.get(device__name=search_string)
        data_list.append(curr_data)

    except ValueError as err:
        sys.stderr.write("{}".format(err))
        #~ return render(request, 'polls/detail.html', {
        #~ 'question': question,
        #~ 'error_message': "You didn't select a choice.",
        #~ })
    else:
        context = {'data_list': data_list}
        return render(request, 'lara_data/results.html', context)
        #return HttpResponseRedirect(reverse('lara_data:results', args=(lara_device_list,)))


def results(request, data_list):
    #question = get_object_or_404(Question, pk=question_id)
    context = {'data_list': data_list}
    return render(request, 'lara_data/results.html', context)


class DataDetailsView(DetailView):
    model = Data


def dataDetails(request, data_id):
    curr_data = get_object_or_404(Data, pk=data_id)
    context = {'data': curr_data}
    return render(request, 'lara_data/data.html', context)

"""
________________________________________________________________________

:PROJECT: LARA

*resetting and initialisation of substance db*

:details: evaluate projects and experiments use:
          
         python3 manage.py eval_data 
         
         

:file:    eval_data.py
:authors: mark doerr (mark@ismeralda.org)
          Stefan Born (stefan.born@tu-berlin.de )

:date: (creation)          20190707
:date: (last modification) 20190713

.. note:: -
.. todo:: - 
________________________________________________________________________
"""
__version__ = "0.0.2"

import os
import logging

from django.core.management.base import BaseCommand, CommandError
from django.core.management import  call_command

from django.conf import settings

from lara_django_data.management.eval_data_db import EvalDataDB

class Command(BaseCommand):
    """see https://docs.djangoproject.com/en/1.9/howto/custom-management-commands/ for more details
    using now new argparse mechanism of django > 1.8
    """    
    logging.basicConfig(format='%(levelname)s| %(module)s.%(funcName)s:%(message)s', level=logging.DEBUG) #level=logging.ERROR
    
    help = 'evaluating data LARA  database.'
    
    def add_arguments(self, parser):
        """ command line arguments s. https://docs.python.org/2/library/argparse.html#module-argparse """
        
        # defining named commandline arguments
        parser.add_argument('-s','--substance-csv',
            action='store',
            dest='substance_csv',
            help='substance csv file for additon or replacement of database entries')
            
        parser.add_argument('--prot-seq',
            action='store',
            help='protein sequence csv file for additon or replacement of database entries')
            
        parser.add_argument('-d','--data',
            action='store',
            help='evaluate data')
            
        parser.add_argument('-r','--replace',
            action='store_true',
            default=False,
            help='replace database entries by new data')
    
    def handle(self, *args, **options):
        """Dispatcher based on commandline arguments/options"""
            
        if options['data'] :
            EvalDataDB( data_name=options['data'])
            
            
            

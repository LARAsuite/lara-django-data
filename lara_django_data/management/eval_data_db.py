"""
________________________________________________________________________

:PROJECT: LARA

*eval data database*

:details:  eval data in LARA database
         - 

:file:    add_substances.py
:authors: mark doerr (mark@ismeralda.org)

:date: (creation)          20190707
:date: (last modification) 20190707

.. note:: -
.. todo:: - 
________________________________________________________________________
"""
__version__ = "0.0.1"

import os
import shutil
import logging
import subprocess
import json
import csv

import numpy as np

from django.db import IntegrityError
from django.core.management.base import BaseCommand, CommandError
from django.core.exceptions import ObjectDoesNotExist

from django.conf import settings

from laralib.bio.proteins import convMutLists2mutStr1LC, convMutLists2mutStr3LC
from laralib.ml.parameter_optimisation import findOptimalRegularisation, summariseResults, print_tabular
from laralib.ml.linear_models import calcCoefficientsLasso, printCoeffs, coefSelectBenHochb

from lara_django_base.models import Namespace

from lara_django_data.models import Data
from lara_django_sequences.models import SequenceClass, Sequence

class EvalDataDB(BaseCommand):
    """Adding substances to django database, could be later expanded to other file formats, 
       in this case, the file opening should be moved to the processing unit """
    def __init__(self, event=None, replace_data=False, data_name=""):
        super().__init__()
        
        logging.debug("eval data now{}".format(1) )
        
        namespace = "de.unigreifswald.biochem.akb" + "."
        
        reference_seq_name = namespace + "proteinasek.reference_seq"
        loci_name = namespace + "proteinasek.subst_loci"
        sp_mutations_name = namespace + "proteinasek.sp_mutations"
        
        features_name = namespace + "proteinasek.features"
        act_data_name = namespace + "proteinasek.activity"
        
        
        # loading protein activity data
        act_data = Data.objects.get(name_full=act_data_name)
        act_dat_JSON =  act_data.data_JSON
        act_data_lst = json.loads(act_dat_JSON)[0]['activities']
        act_data_arr = np.array(act_data_lst)
        
        y = act_data_arr[:59]  # subset of activity data for machine learning 

        # retrieving the sequences
        
        ## reference sequence (= starting point/scaffold of activity improvements)
        ref_seq = Sequence.objects.all().first().sequence
        logging.debug("ref seq: {}".format(ref_seq) )
        
        # iterate through sequences and find mutations
        # generate mutation dictionary and feature array 
        
        ref_seq_select_JSON = substitutions_JSON = Data.objects.get(name_full=reference_seq_name).data_JSON
        
        #~ logging.debug("ref seq selct: {}".format(ref_seq_select_JSON) )
        
        ref_seq_select = json.loads(ref_seq_select_JSON)[0]['ref_sequence']
        
        ref_seq_select_arr = np.array(ref_seq_select)
        print (ref_seq_select_arr)
        
        i = 0
        min_left = 0 
        max_right = len(ref_seq)
        
        for curr_seq_dict in Sequence.objects.all():
            curr_seq = curr_seq_dict.sequence
            #~ print(curr_seq)
            if i == 5: break
            i += 1
            
        # ... to be continued ...
        
        # loading loci information
        
        loci_JSON = Data.objects.get(name_full=loci_name).data_JSON
        loci = json.loads(loci_JSON)[0]['loci']
        loci_str_arr = np.array(loci)
        #~ logging.debug("loci str: {}".format(loci_str_arr) )
        
        # loading substitutions = single point mutations
        
        substitutions_JSON = Data.objects.get(name_full=sp_mutations_name).data_JSON
        substitutions = json.loads(substitutions_JSON)[0]['substitutions']
        #~ logging.debug("subst:{}".format(substitutions) )
        substitutions_arr = np.array(substitutions)
        
        # loading features = X from CSV file stored at a location with path from Database
        
        act_features_filename = str(Data.objects.get(name_full=features_name).filename)
        act_features_filename_full = os.path.join( settings.DJANGO_MEDIA_PATH, act_features_filename )
        logging.debug("feat filename {}".format(act_features_filename_full) )
        
        X = np.genfromtxt(act_features_filename_full,delimiter=',')[:59,:]
        
        #~ print(X)
        
        # one and three letter mutation strings
        subst_names1L = convMutLists2mutStr1LC(ref_seq_select_arr,loci_str_arr,substitutions_arr)
        
        subst_names3L = convMutLists2mutStr3LC(ref_seq_select_arr,loci_str_arr,substitutions_arr)
        
        print(subst_names3L)
        
        
        """ Applying linear models to the data
            Lasso (for sparse solutions) -- especially for higher order models with only 59/.../96 measurements the onlye
            reasonable models to be learned are sparse. For the Lasso the right degree of sparsenesse/the right regularization has to be found out. 
            The same is true of all other methods (SVR,...).
        """
        
        opti_results_1 = findOptimalRegularisation(X,y)
        #~ print(opti_results_1)
        
        # output just for control
        #~ alphas, mae_means, mae_stds, mae_log_means, mae_log_stds = summariseResults(opti_results_1)
        
        alphas, means, stds, means_log, stds_log = summariseResults(opti_results_1)
        print_tabular(alphas,means, stds)
        print('------')
        print_tabular(alphas, means_log, stds_log)
        # add saving step into database
        
        # select best alphas
        
        min_alpha_idx = np.where(means_log == np.amin(means_log))[0][0]
        #min_alpha_idx[0][0]
        opt_alpha1= alphas[min_alpha_idx]
        interval_steps = 10**(np.log10(opt_alpha1)*2)
        print(interval_steps)
        alphas = np.arange(opt_alpha1/2, opt_alpha1*2, interval_steps)
        
        # .... should be further automated ...
        
        # refine regularisation by using best alphas
        opti_results_2 = findOptimalRegularisation(X,y, alphas=alphas) # np.arange(0.04,0.2,0.01)
        alpha, means, stds, means_log, stds_log = summariseResults(opti_results_2)
        print_tabular(alpha, means, stds)
        print('-----------')
        print_tabular(alpha, means_log, stds_log)
        
        # get best alpha
        
        min_alpha_idx = np.where(means_log == np.amin(means_log))[0][0]
        opt_alpha2 = alpha[min_alpha_idx]
        
        logging.debug("best alpha after refinement: {}".format(opt_alpha2) ) 
        
        
        # now the core machine learning part
        
        # learning with a subset of the features, 500 iterations
        mean_coefs, std_coefs = calcCoefficientsLasso(X, y, alpha=opt_alpha2, num_iter=500, labels=subst_names3L )
        
        print(mean_coefs)
        
        # selecting good coefficients
        coefSelectBenHochb(mean_coefs, std_coefs, labels=subst_names3L, alpha = 0.2 )
        # this shall be returned into the database and displayed as HTML

                
        """ 
        In principle these weights can be used for a variable selection for the rest of the process. It would be wise,
        however to use a sound statistical procedure instead of the rather adhoc-ish variant of the paper in question.

        Given the means and standard deviations of the coefficients (or rather given the coefficients for N simulation runs) we can use a false discover rate/Benjamini-Hochberg procedure to guarantee that at most a certain percentage $\alpha$ of the 
        accepted coefficients has a true mean<=0. 

        Or one could throw out the bad values with an FDR of a certain percentage $\alpha$ to guarantee that at most $\alpha$ of the thrown out coefficients should have been kept.
        """

"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_data urls *

:details: lara_django_data urls module.
         - add app specific urls here
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: -
________________________________________________________________________
"""


from django.conf import settings
from django.conf.urls.static import static
from django.urls import path, include
from django.views.generic import TemplateView

#import lara_django.urls as base_urls

# from .grpc_if.lara_data_pb2_grpc import add_DataControllerServicer_to_server
# from .grpc_if.services import DataService


from . import views

# Add your {cookiecutter.project_slug}} urls here.


# !! this sets the apps namespace to be used in the template
app_name = "lara_django_data"

# companies and institutions should also be added
# the 'name' attribute is used in templates to address the url independent of the view

urlpatterns = [
    path('data/list/', views.DataSingleTableView.as_view(), name='data-list'),
    path('data/create/', views.DataCreateView.as_view(), name='data-create'),
    path('data/update/<uuid:pk>', views.DataUpdateView.as_view(), name='data-update'),
    path('data/delete/<uuid:pk>', views.DataDeleteView.as_view(), name='data-delete'),
    path('data/<uuid:pk>/', views.DataDetailView.as_view(), name='data-detail'),
    path('', views.DataSingleTableView.as_view(), name='data-root'),
    
    # path('', views.IndexView.as_view(), name='index'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

# def grpc_handlers(server):
#     add_DataControllerServicer_to_server(DataService.as_servicer(), server)

"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_data admin *

:details: lara_django_data admin module admin backend configuration.
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - run "lara-django-dev admin_generator lara_django_data >> admin.py" to update this file
________________________________________________________________________
"""
# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import ExtraData, Visualisation, Data, Evaluation


@admin.register(ExtraData)
class ExtraDataAdmin(admin.ModelAdmin):
    list_display = (
        'data_type',
        'namespace',
        'URI',
        'text',
        'XML',
        'JSON',
        'bin',
        'media_type',
        'IRI',
        'URL',
        'description',
        'extra_data_id',
        'file',
        'image',
    )
    list_filter = ('data_type', 'namespace', 'media_type')


@admin.register(Visualisation)
class VisualisationAdmin(admin.ModelAdmin):
    list_display = (
        'visualisation_id',
        'namespace',
        'name',
        'version',
        'name_full',
        'data_type',
        'plotting_method',
        'plotting_procedure',
        'rendering_html_template',
        'rendering_css_template',
        'rendering_component',
        'IRI',
        'caption',
        'thumbnail',
        'description',
    )
    list_filter = (
        'namespace',
        'data_type',
        'plotting_method',
        'plotting_procedure',
    )
    raw_id_fields = ('tags',)
    search_fields = ('name',)


@admin.register(Data)
class DataAdmin(admin.ModelAdmin):
    list_display = (
        'data_id',
        'data_type',
        'media_type',
        'namespace',
        'name',
        'version',
        'name_full',
        'UUID',
        'hash_SHA256',
        'datetime_created',
        'method',
        'process',
        'process_calibration',
        'data_JSON',
        'data_XML',
        'data_BLOB',
        'data_calibration_BLOB',
        'metadata',
        'URI',
        'IRI',
        'handle',
        'DOI',
        'filename',
        'literature',
        'icon',
        'description',
    )
    list_filter = (
        'data_type',
        'media_type',
        'namespace',
        'datetime_created',
        'method',
        'process',
        'process_calibration',
    )
    raw_id_fields = ('visualisations', 'tags')
    search_fields = ('name',)


@admin.register(Evaluation)
class EvaluationAdmin(admin.ModelAdmin):
    list_display = (
        'eval_id',
        'namespace',
        'name',
        'name_full',
        'version',
        'UUID',
        'hash_SHA256',
        'parameters',
        'eval_method',
        'eval_procedure',
        'URI',
        'IRI',
        'handle',
        'DOI',
        'literature',
        'caption',
        'description',
    )
    list_filter = ('namespace', 'eval_method', 'eval_procedure')
    raw_id_fields = (
        'parent_evals',
        'eval_data',
        'results_data',
        'visualisations',
        'tags',
    )
    search_fields = ('name',)
